<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

const MAX_TOTAL = 280;

class Refresh extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        while(true){
            $this->handle2();
            sleep(30);
        }
    }

    private function handle2(){
        $last_crawled_post_id = \App\Models\Ad::first()->last_crawled_post_id;

        $posts = $this->get_new_posts($last_crawled_post_id);
        
        if(!$posts){
            return 0;
        }
        
        \App\Models\Ad::first()->update(['last_crawled_post_id' => $posts[0]['token']]);
        
        foreach($posts as $post){
            $description = str_replace(['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'], range(0, 10), $post['description']);
            $description = explode("\n", $description);

            preg_match('/ودیعه: (.*) تومان/', $description[0], $m);
            $pre_rent = (str_replace(',', '', $m[1] ?? 0)) / 1000000;

            preg_match('/اجاره ماهیانه: (.*) تومان/', $description[1], $m);
            $rent = (str_replace(',', '', $m[1] ?? 0)) / 1000000;

            $total = 30 * $rent + $pre_rent;
            
            if($total < MAX_TOTAL){
                $this->send_telegram_message($post, $rent, $pre_rent, $total);
            }
        }
        
        return 0;
    }

    private function get_first_page() {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.divar.ir/v8/web-search/tehran/rent-apartment?credit=-280000000&rent=-8000000&parking=true&rent_to_single=true',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
              'authority: api.divar.ir',
              'sec-ch-ua: "Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
              'accept: application/json, text/plain, */*',
              'sec-ch-ua-mobile: ?0',
              'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36',
              'origin: https://divar.ir',
              'sec-fetch-site: same-site',
              'sec-fetch-mode: cors',
              'sec-fetch-dest: empty',
              'referer: https://divar.ir/',
              'accept-language: en-US,en;q=0.9,fa;q=0.8',
              'cookie: _gcl_au=1.1.1426951860.1616416885; did=34eb2c84-d535-4734-9bb1-10e5a3c04786; device_id=3747475723; _ga=GA1.2.793831046.1616480888; _gid=GA1.2.1822523616.1617788612; last_loc=no; sessionid=78xs0spwfd7kt7c6d3yc7u812pgc5xjh; token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiMDk5NjAwMDAwMDMiLCJleHAiOjE2MTk2OTY2MzguNTMwOTM1LCJ2ZXJpZmllZF90aW1lIjoxNjE4NDAwNjM4LjUzMDkyNiwidXNlci10eXBlIjoicGVyc29uYWwiLCJ1c2VyLXR5cGUtZmEiOiJcdTA2N2VcdTA2NDZcdTA2NDQgXHUwNjM0XHUwNjJlXHUwNjM1XHUwNmNjIn0.qNIVYFGSeVD9WZcOlPCPsIJWInAD0yHgO0G4JfL73-4; EOAuthToken=2D15NT2J0ZtY9jTuHZQo37vZkzSovBaBqKW0IbpVq8M=; EOAuthUserInfo=bDrOPQe3iA9L3zrvULxLYI1WSgl3d/PUhrpKWQ9qLa9zOeYoA6imCzjmIsoqIDTRkpZQQ3xNj/20F21npzSvErKtba8s5ILVW9q/XhePN1A=; city=tehran; city=tehran'
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);

        curl_close($curl);
        return $response;
    }

    private function get_next_page($last_post_date) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.divar.ir/v8/search/1/apartment-rent',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{"json_schema":{"category":{"value":"apartment-rent"},"credit":{"max":280000000},"rent":{"max":8000000},"parking":true,"rent_to_single":true},"last-post-date":' . $last_post_date . '}',
            CURLOPT_HTTPHEADER => array(
              'authority: api.divar.ir',
              'sec-ch-ua: "Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
              'accept: application/json, text/plain, */*',
              'sec-ch-ua-mobile: ?0',
              'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36',
              'content-type: application/json;charset=UTF-8',
              'origin: https://divar.ir',
              'sec-fetch-site: same-site',
              'sec-fetch-mode: cors',
              'sec-fetch-dest: empty',
              'referer: https://divar.ir/',
              'accept-language: en-US,en;q=0.9,fa;q=0.8',
              'cookie: _gcl_au=1.1.1426951860.1616416885; did=34eb2c84-d535-4734-9bb1-10e5a3c04786; device_id=3747475723; _ga=GA1.2.793831046.1616480888; _gid=GA1.2.1822523616.1617788612; last_loc=no; sessionid=78xs0spwfd7kt7c6d3yc7u812pgc5xjh; token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiMDk5NjAwMDAwMDMiLCJleHAiOjE2MTk2OTY2MzguNTMwOTM1LCJ2ZXJpZmllZF90aW1lIjoxNjE4NDAwNjM4LjUzMDkyNiwidXNlci10eXBlIjoicGVyc29uYWwiLCJ1c2VyLXR5cGUtZmEiOiJcdTA2N2VcdTA2NDZcdTA2NDQgXHUwNjM0XHUwNjJlXHUwNjM1XHUwNmNjIn0.qNIVYFGSeVD9WZcOlPCPsIJWInAD0yHgO0G4JfL73-4; EOAuthToken=2D15NT2J0ZtY9jTuHZQo37vZkzSovBaBqKW0IbpVq8M=; EOAuthUserInfo=bDrOPQe3iA9L3zrvULxLYI1WSgl3d/PUhrpKWQ9qLa9zOeYoA6imCzjmIsoqIDTRkpZQQ3xNj/20F21npzSvErKtba8s5ILVW9q/XhePN1A=; city=tehran; city=tehran'
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);

        curl_close($curl);
        return $response;

    }

    private function send_telegram_message($post, $rent, $pre_rent, $total){
        $curl = curl_init();

        $text = $post['title'] . PHP_EOL . $pre_rent . PHP_EOL. $rent . PHP_EOL . $total . PHP_EOL . "https://divar.ir/v/{$post['token']}";

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://api.telegram.org/bot1712472453:AAF4PQg0WXSKl97tFTgtXDHTJHedVUKlWJA/sendMessage',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array('text' => $text,'chat_id' => '80891987'),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;

    }

    private function get_new_posts($last_crawled_post_id) {
        $first_page = $this->get_first_page(); 
        $last_post_date = $first_page['last_post_date'];

        $done = false;
        $new_posts = [];
        
        foreach($first_page['widget_list'] as $post) {
            if($post['data']['token'] == $last_crawled_post_id){
                $done = true;
                break;
            }
            $new_posts[] = $post;
        }
        
        while(!$done) {
            $page = $this->get_next_page($last_post_date);
            $last_post_date = $page['last_post_date'];

            foreach($page['widget_list'] as $post) {
                if($post['data']['token'] == $last_crawled_post_id){
                    $done = true;
                    break;
                }
                $new_posts[] = $post;
            }
        }

        return array_column($new_posts, 'data');
    }
}
